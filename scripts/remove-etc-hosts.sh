#!/usr/bin/env bash

# ****************************************
# * Add/Update a Hostname/IpAddress entry
# * to the /etc/hosts file.
# ****************************************

# PATH TO YOUR HOSTS FILE
ETC_HOSTS=/etc/hosts

# ARGUMENTS
HOSTNAME=$1

function removehost() {
    if [ -n "$(grep $HOSTNAME $ETC_HOSTS)" ]
    then
        echo "$HOSTNAME Found in your $ETC_HOSTS, Removing now...";
        #sed -i".bak" "/$HOSTNAME/d" $ETC_HOSTS
        flock -x -w 5 $ETC_HOSTS -c "sed -i\".bak\" \"/$HOSTNAME/d\" $ETC_HOSTS"
    else
        echo "$HOSTNAME was not found in your $ETC_HOSTS";
    fi
}

removehost;
