#!/usr/bin/env bash

# Set working directory to script path
cd "$(dirname "$0")"

# Exit on Error
set -e

## ****************************************
## Registers a new node with torque using
## the `qmgr -c 'create node'` command.
## ****************************************
HOSTNAME=$1
TTL=$2
REQUEST_ID=$3
IPADDRESS=$4

if [ -z $IPADDRESS ]
then
    echo "********************************************"
    echo "***** WARNING: Invalid Node IP Address *****"
    echo "********************************************"
    exit
fi

echo ""
echo "Updating /etc/hosts file..."
echo "----------------------------------------"
echo "    hostname: $HOSTNAME"
echo "          ip: $IPADDRESS"
echo "----------------------------------------"
./add-etc-hosts.sh $HOSTNAME $IPADDRESS

echo ""
echo "Adding node to the cluster..."
echo "----------------------------------------"
echo "    hostname: $HOSTNAME"
echo "         ttl: $TTL"
echo "  request_id: $REQUEST_ID"
echo "----------------------------------------"
echo "Executing: /usr/local/bin/qmgr -c \"create node $HOSTNAME np=1,TTL=$TTL,requestid=$REQUEST_ID\""
echo "----------------------------------------"

#/usr/local/bin/qmgr -c "create node $HOSTNAME np=1,TTL=$TTL,requestid=$REQUEST_ID"
/usr/local/bin/qmgr -c "create node $HOSTNAME TTL=$TTL,requestid=$REQUEST_ID"
/usr/local/bin/qmgr -c "set node $HOSTNAME properties += cloud"

echo "----------------------------------------"
echo ""
echo "Successfully added node to the cluster."
echo ""
