#!/usr/bin/env bash

# ****************************************
# * Add/Update a Hostname/IpAddress entry
# * to the /etc/hosts file.
# ****************************************

# PATH TO YOUR HOSTS FILE
ETC_HOSTS=/etc/hosts

# ARGUMENTS
HOSTNAME=$1
IP=$2

function addhost() {
    # HOSTNAME=$1
    HOSTS_LINE="$IP  $HOSTNAME"
    if [ -n "$(grep $HOSTNAME /etc/hosts)" ]
        then
            echo "$HOSTNAME already exists : $(grep $HOSTNAME $ETC_HOSTS)"
        else
            echo "Adding $HOSTNAME to your $ETC_HOSTS";
            echo "$HOSTS_LINE" >> $ETC_HOSTS;

            if [ -n "$(grep $HOSTNAME /etc/hosts)" ]
                then
                    echo "$HOSTNAME was added successfully \n $(grep $HOSTNAME /etc/hosts)";
                else
                    echo "ERROR: Failed to add $IP\t$HOSTNAME";
            fi
    fi
}

addhost;