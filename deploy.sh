#!/usr/bin/env bash

# Provision Torque Node Instance with OpenVPN connection
#STACK_ID=d09bd9e8-56a6-4f65-8442-20dcc8578db4
#STACK_ID="a1238cc6-d2f7-4af8-9fd6-5e4d7ed523c9"
#STACK_ID="3f03a8ab-e221-4534-9232-24f5b4e6c171"
STACK_ID="417bec47-13a1-4222-ad54-e16063f90329"
TORQUE_SERVER_IPADDRESS=`ip route get 8.8.8.8 | awk 'NR==1 {print $NF}'`
TORQUE_SERVER_HOSTNAME=`hostname -f`

NODE_NAME="$1"
NODE_INSTANCE=`nodus-run cluster/create.js $NODE_NAME $STACK_ID args.torque_server_ipaddress="$TORQUE_SERVER_IPADDRESS" args.torque_server_hostname="$TORQUE_SERVER_HOSTNAME"`

echo
echo $NODE_INSTANCE | jq
echo

NODE_ID=`echo $NODE_INSTANCE | jq -r .id`
NODE_SSH=`echo $NODE_INSTANCE | jq -r .ssh`
NODE_IPADDRESS=`echo $NODE_INSTANCE | jq -r .vpn_ipAddress`
#NODE_CONNECT=".nodus/cluster/$NODE_ID/connect.sh"
#NODE_CONNECT="ssh -o \"StrictHostKeyChecking no\" -i keypair.pem $NODE_SSH"
NODE_CONNECT="./connect.sh $NODE_SSH"
NODE_HOSTNAME=`$NODE_CONNECT hostname -f`

#echo $NODE_INSTANCE
echo "Successfully deployed new node instance..."
echo "  instance=$NODE_INSTANCE"
echo "  id=$NODE_ID"
echo "  ipAddress=$NODE_IPADDRESS"
echo "  hostname=$NODE_HOSTNAME"

## Update /etc/hosts file
echo "Updating /etc/hosts file..."
sudo -- sh -c "echo $NODE_IPADDRESS $NODE_HOSTNAME >> /etc/hosts"

## Call qmgr command
sudo /usr/local/bin/qmgr -c "create node $NODE_HOSTNAME"
sudo systemctl restart pbs_server
