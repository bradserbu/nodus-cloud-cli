'use strict';

// ** Constants
const DEFAULT_NODUS_URL='http://app.nodusplatform.com:8080';

// ** Dependencies
const request = require('request-promise');
const logger = require('nodus-framework').logging.createLogger('nodus');
const errors = require('nodus-framework').errors;
const files = require('nodus-framework').files;

function build(filename) {
    logger.debug('BUILD', arguments);    

    logger.info('Loading stack...', {filename});
    const stack = files.requireFile(filename);

    const nodus_url = process.env['NODUS_URL'] || DEFAULT_NODUS_URL;
    logger.debug('NODUS_URL', nodus_url);

    const endpoint = {
        uri: `${nodus_url}/api/stack/build`,
        json: {stack}
    };
    logger.debug('ENDPOINT', endpoint);
    
    return request
        .post(endpoint)
        .then(response => response.data);
}

// ** Exports
module.exports = build;