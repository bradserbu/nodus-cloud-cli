'use strict';

// ** Constants
const DEFAULT_NODUS_URL = 'http://app.nodusplatform.com:8080';
const DEFAULT_TIMEOUT = 240000; // 4 minutes

// ** Dependencies
const _ = require('lodash');
const Promise = require('bluebird');
const request = require('request-promise');
const logger = require('nodus-framework').logging.createLogger();
const errors = require('nodus-framework').errors;
const Handlebars = require('handlebars');
const execa = require('execa');
const GenId = require('gen-id');
const zpad = require('zpad');

function Node() {
    return Promise.resolve({
        id: '79e4a1c7-7e08-4629-bfb4-d6ca710e1b70',
        name: 'test-cluster-ASFY-01',
        stack: '15af0556-8641-4a9c-98e8-a0f575c0753c',
        status: 'status',
        ipAddress: '54.197.39.222',
        ssh: 'centos@54.197.39.222',
        vpn_ipAddress: '172.24.3.250',
        hostname: 'test-cluster-ASFY-01.ec2.internal'
    });
}

function Command(cmd, args) {

    const src = cmd;

    const template = Handlebars.compile(src);
    const command = template(args);

    logger.debug('RUN', command);

    return command;
}

function createNode(name, stack, args, on) {

    logger.info('CREATE_NODE', {name, stack, args});

    // Check required arguments
    if (!name) throw errors('REQUIRED_ARGUMENT', 'The "name" argument is required.');
    if (!stack) throw errors('REQUIRED_ARGUMENT', 'The "stack" argument is required.');

    const nodus_url = process.env['NODUS_URL'] || DEFAULT_NODUS_URL;
    logger.debug('NODUS_URL', nodus_url);

    const endpoint = {
        uri: `${nodus_url}/api/cluster/create`,
        qs: {name, stack, args},
        json: true,
        timeout: DEFAULT_TIMEOUT
    };
    logger.debug('ENDPOINT', endpoint);

    logger.info('Creating instance...', {name, stack, args, on});
    return request
        .get(endpoint)
        .then(response => response.data)
        .then(instance => {
            logger.info('Successfully created instance...', instance);

            const status = instance.status;
            if (!status)
            {
                logger.error('NO_STATUS', 'Instance created without a "status" property.', instance);
                return instance;
            }

            if (on[status]) {
                logger.info("Running on status trigger...", {instance, handler: on[status]});

//                run(on['status'], {instance});
                const cmd = Command(on[status], instance);
                logger.debug('CMD', cmd);

                return execa
                    .shell(cmd)
                    .then(result =>
                        // Log command output TODO: Save output to file
                        logger.info(`
exec: ${cmd}
------------------------------------------------
${result.stdout}
------------------------------------------------
`))
                    .then(_ => instance);                                   // Always return the instance
            }

            return instance;
        })
        // ** Save node in database
        // .then(instance => Nodes.add(instance));
}

function create(name, stack, args, nodes, on) {

    if (!name) throw errors('REQUIRED_ARGUMENT', 'The "name" argument is required.');
    if (!stack) throw errors('REQUIRED_ARGUMENT', 'The "stack" argument is required.');

    args = args || {};
    on = on || {};

    //const id_prefix = shortid.generate();
    const id_prefix = GenId('XXXX').generate().toUpperCase();

    const NumDigits = num => Math.log(num) * Math.LOG10E + 1 | 0;  // for positive integers

    if (nodes) {
        nodes = parseInt(nodes, 10);

        const num_digits = NumDigits(nodes);
        const padding = (num_digits > 1)
            ? num_digits
            : 2; // Min two digits

        // ** Create N number of nodes
        const promises = _.range(1, nodes + 1).map(num => {

            const num_prefix = zpad(num, padding);
            const instance_name = `${name}-${id_prefix}-${num_prefix}`;

            return createNode(instance_name, stack, args, on);
        });

        // ** Wait until they have all completed
        return Promise.all(promises);
    } else {
        const instance_name = `${name}-${id_prefix}`;
        return createNode(instance_name, stack, args, on);
    }
}

// ** Exports
module.exports = create;