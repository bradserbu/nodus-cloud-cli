'use strict';

// ** Constants
const DEFAULT_NODUS_URL='http://app.nodusplatform.com:8080';

// ** Dependencies
const request = require('request-promise');
const logger = require('nodus-framework').logging.createLogger('nodus');
const errors = require('nodus-framework').errors;

function destroy(id, hostname) {
    logger.debug('DESTROY', arguments);

    const nodus_url = process.env['NODUS_URL'] || DEFAULT_NODUS_URL;
    logger.debug('NODUS_URL', nodus_url);
    
    const endpoint = {
        uri: `${nodus_url}/api/cluster/destroy`,
        qs: {id, hostname},
        json: true
    };
    logger.debug('ENDPOINT', endpoint);

    logger.info('Destroying cluster...', {id});
    return request
        .get(endpoint)
        .then(response => response.data);
}

// ** Exports
module.exports = destroy;