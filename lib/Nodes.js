'use strict';

// ** Dependencies
const DB = require('./db');

const AddNode = node => DB()
    .then(db => db
        .get('nodes')
        .push(node)
        .write()
        .then(_ => node)
    );

const RemoveNode = node => DB()
    .then(db => db
        .get('nodes')
        .remove(node)
        .write()
        .then(_ => node)
    );

// ** Exports
module.exports = {
    list: () => DB().then(db => db.get('nodes').value()),
    add: AddNode,
    remove: node => RemoveNode(node)
};