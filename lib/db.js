'use strict';

// ** Dependencies
const util = require('util');
const low = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');
const FileAsync = require('lowdb/adapters/FileAsync');
const Path = require('path');
const files = require('nodus-framework').files;
const logger = require('nodus-framework').logging.createLogger();
const fs = require('fs-extra');

// ** Constants
const NODUS_HOME = files.resolvePath('~/.nodus');

const CreateDirectory = dirpath => fs
    .ensureDir(dirpath)
    .then(_ => dirpath);

const Database = name => {

    name = name || 'db';

    // Ensure the NODUS_HOME directory exists
    const NodusHome = () => CreateDirectory(NODUS_HOME);

    return NodusHome()
        .then(home_dir => {
            logger.debug('HOME_DIR', home_dir);

            const db_path = Path.join(NODUS_HOME, `${name}.json`);

            logger.debug('Opening Database...', db_path);
            const adapter = new FileAsync(db_path);
            const db = low(adapter);

            return db;
        })
        // ** Set Defaults
        .then(db => db
            .defaults({nodes: []})
            .write()
            .then(_ => db)
        )
};

// ** Exports
module.exports = Database;