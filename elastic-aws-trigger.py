#!/usr/bin/env python

import sys, getopt
import subprocess
import time
from datetime import datetime, timedelta
from subprocess import call
import socket
import json
import hashlib
import os
import uuid

# VPN using UDP
#stack_id = '5d14136e-c32f-4336-8ad6-3e159c8b7770'

# VPN using TCP
#stack_id = '2a9dacb7-d3d7-4076-b5db-4063ee95d37f'

# VPN using TCP
#stack_id = '29f906bd-6c85-4445-aa56-b51831b8184c'

# nodus on ecdemo-insight.ac
stack_id = '3ae6f075-ab29-4602-a4a8-3782c9854b2a'

class aws_options(object):

    #class vars
    def __init__(self):
        self.__node_to_remove = ''
        self.__request_geometry = ''

    # getters/setters
    def set_node_to_remove(self, node_name):
        self.__node_to_remove = node_name

    def get_node_to_remove(self):
        return self.__node_to_remove

    def set_request_geometry(self, request_geometry):
        self.__request_geometry = request_geometry

    def get_request_geometry(self):
        return self.__request_geometry


def parse_request_geometry(request_geometry):
    try:
        d = request_geometry.split('@')
        assert len(d) == 2
        node_count = int(d[0])
        assert node_count > 0
        request_moab_time = d[1]
    except:
        die("Request geometry '%s' is invalid. Please start with a positive number of nodes followed by the @ symbol followed by the time to live." % request_geometry)

    try:
        d = map(int, request_moab_time.split(':'))
        for i in d:
            assert i >= 0
        assert len(d) <= 4
    except:
        die("Request geometry '%s' is invalid. The time to live should be up to 4 non-negative integers separated by colons." % request_geometry)
    while len(d) < 4:
        d.insert(0,0)

    today = datetime.utcnow()
    #Add 1 minute of pading to the TTL
    moab_time = timedelta(days=d[0], hours=d[1], minutes=d[2] + 1, seconds=d[3])
    expected_retire_date = today + moab_time
    return (node_count, expected_retire_date.strftime("%Y-%m-%dT%T+00"))


def run_system_command(cmd):
    print "Running command: \"%s\"" % cmd

    try:
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
        (output, err) = p.communicate()
        p_status = p.wait()
    except os.TypeError:
        print "Error running command: " + cmd
        sys.exit(2)

    print "Output = " + output

    return(output)


def add_nodes(request_geometry):
    print "Request Geometry = " + request_geometry
    node_count, ttl = parse_request_geometry(request_geometry)

    #node_prefix="elastic-cloud"
    node_prefix="aws"
    hostname = socket.gethostname()
    ip_address = socket.gethostbyname(hostname)

    # Generate a request_id
    #request_id = abs(hash(str(uuid.uuid4()))) % (10 ** 8)

    # On Deployed Event Hook
    on_deployed = '/opt/moab/contrib/nodus-cloud-cli/scripts/add-node.sh {{hostname}} %s {{id}} {{vpn_ipAddress}}' % (ttl)

    # nodus cmd
    nodus_run_cmd = "/usr/bin/nodus-run /opt/moab/contrib/nodus-cloud-cli/cluster/create.js nodes=%d name=%s stack=%s args.torque_sleep=0 args.torque_server_ipaddress=%s args.torque_server_hostname=%s on.deployed='%s'" % (node_count, node_prefix, stack_id, ip_address, hostname, on_deployed)
    # -- Pre on.instance Hook: nodus_run_cmd = "/usr/bin/nodus-run /opt/moab/contrib/nodus-cloud-cli/cluster/create.js nodes=%d name=%s stack=%s args.torque_sleep=0 args.torque_server_ipaddress=%s args.torque_server_hostname=%s" % (node_count, node_prefix, stack_id, ip_address, hostname)
    #nodus_run_cmd = "./nodus-run cluster/create.js nodes=%d name=%s stack=%s args.torque_server_ipaddress=%s args.torque_server_hostname=%s" % (node_count, node_prefix, stack_id, ip_address, hostname)
    # nodus call - this is a blocking call, so we wait for it to return
    nodus_output = run_system_command(nodus_run_cmd)

    # parse JSON response
    nodus_json_obj = json.loads(nodus_output)
    #request_id = abs(hash(nodus_json_obj[0]['id'])) % (10 ** 8)

    # create a unique hash based off the first hostname for the request id
    for node in nodus_json_obj:
        if (node['status'] != "deployed"):
            print "Warning: invalid node status: %s, %s - %s" % (node['name'], node['id'], node['status'])
            #remove_node(node['hostname'])
            continue

        if not node['vpn_ipAddress']:
            print "Warning: missing VPN address: %s, %s" % (node['name'], node['id'])
            #remove_node(node['hostname'])
            continue

        # add IP/hostname to /etc/hosts file
        # TODO: remove for phase 2
        #hosts_line = node['vpn_ipAddress'] + " " + node['hostname'] + "\n"
        #hosts_file = open("/etc/hosts", "a+")
        #hosts_file.write(hosts_line)
        #hosts_file.close()

        # create users
        # TODO: remove for phase 3
        #group_user_add = '/opt/moab/contrib/nodus-cloud-cli/connect.sh centos@' + node['vpn_ipAddress'] + ' "sudo groupadd jedi -g 10000; sudo groupadd sith -g 10001; sudo useradd -m -u 10000 -g 10000 yoda; sudo useradd -m -u 10001 -g 10001 dvader; sudo useradd -m -u 10002 -g 10000 mwindu; sudo useradd -m -u 10003 -g 10000 lskywalker; sudo useradd -m -u 10004 -g 10001 dmaul"'
        #run_system_command(group_user_add)

        # running ls makes the hope dir appear
        #create_dirs = '/opt/moab/contrib/nodus-cloud-cli/connect.sh "su yoda -c "ls /home/yoda"; su lskywalker -c "ls /home/lskywalker"; su mwindu -c "ls /home/mwindu"; su dvader -c "ls /home/dvader"; su dmaul -c "ls /home/dmaul"'
        #run_system_command(create_dirs)

        # qmgr call
        #print "hostname=%s ttl=%s request_id=%d" % (node['hostname'], ttl, request_id)
        #qmgr_cmd = '/usr/local/bin/qmgr -c "create node %s np=1,TTL=%s,requestid=%d"' % (node['hostname'], ttl, request_id)
        #run_system_command(qmgr_cmd)


def remove_node(node_name):
    print "Node to be removed " + node_name

    # nodus call
    nodus_run_cmd = "/usr/bin/nodus-run /opt/moab/contrib/nodus-cloud-cli/cluster/destroy.js hostname=%s &" % node_name
    os.system(nodus_run_cmd)
    #nodus_run_cmd = "/usr/bin/nodus-run /opt/moab/contrib/nodus-cloud-cli/cluster/destroy.js hostname=%s" % node_name
    #run_system_command(nodus_run_cmd)

    # qmgr call
    qmgr_cmd = '/usr/local/bin/qmgr -c "delete node %s"' % node_name
    run_system_command(qmgr_cmd)

    # remove entry from /etc/hosts
    remove_etc_hosts_cmd = '/opt/moab/contrib/nodus-cloud-cli/scripts/remove-etc-hosts.sh %s' % node_name;
    run_system_command(remove_etc_hosts_cmd)

    # TODO remove for phase 2
    #hosts_file = open("/etc/hosts", "r")
    #lines = hosts_file.readlines()
    #hosts_file.close()
    #hosts_file = open("/etc/hosts", "w")
    #for line in lines:
    #    # don't write back the line that matches hostname
    #    if line.find(node_name) == -1:
    #        hosts_file.write(line)
    #hosts_file.close()


def print_usage():

    print "Usage:    elastic-aws-trigger.py [ [ -a request_geometry ] | [ -r node ] ]"
    print "request_geometry = [ num_server@duration ]"


def parse_cmd_line_args(argv, aws_opt):

    try:
        opts, args = getopt.getopt(argv,'a:hr:')
    except getopt.GetoptError:
        print_usage()
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-a':
            aws_opt.set_request_geometry(arg)
        elif opt == '-h':
            print_usage()
            sys.exit(0)
        elif opt == '-r':
            aws_opt.set_node_to_remove(arg)
        else:
            print_usage()
            sys.exit(0)


def main(argv):

    aws_opt = aws_options()

    parse_cmd_line_args(argv, aws_opt)

    if aws_opt.get_request_geometry():
        add_nodes(aws_opt.get_request_geometry())

    if aws_opt.get_node_to_remove():
        remove_node(aws_opt.get_node_to_remove())

    sys.exit(0)

if __name__ == '__main__':
    main(sys.argv[1:])

