#!/usr/bin/env bash

# Exit on Error
set -e

# ****************************************
# * Install script to setup and configure
# * the Moab/NODUS Cloud Bursting
# ****************************************

## Install Node.JS

## Install nodus-run
function InstallPackage() {
    package=$1
    echo
    echo "Installing $package...";
    echo "----------------------------------------"
    npm install -g nodus-framework
    echo "----------------------------------------"
    echo "Successfully installed $package"
    echo
}

# Install NODUS
InstallPackage nodus-framework
InstallPackage nodus

echo ""
echo "Installed: nodus-run"
echo "  version: $( nodus-run --version )"
echo ""
echo "Installed: nodus"
echo "  version: $( nodus --version )"
echo ""