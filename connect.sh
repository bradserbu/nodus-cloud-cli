#!/usr/bin/env bash

SERVER=$1
ARGS=$@

ssh -o "StrictHostKeyChecking no" -i keypair.pem $ARGS
